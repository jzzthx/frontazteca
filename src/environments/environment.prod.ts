export const environment = {
  production: true,
  dominio : {
    host: "https://javatest-eldc-test.java.us2.oraclecloudapps.com/app-banco-azteca-frontend"
  },
  apiRest : {
    //host: "https://bancoaztecaremesaswebapihg.azurewebsites.net/api"    
    //host: "http://10.105.0.8:9000/api"


    host: "https://serviciosazure.abdigital.com:7443/ServiceRemesasBazQa/api"
 // host: "https://wsprodazure.abdigital.com:7443/ServiceRemesasPrd/api"
  },
  apiExt:{
   host:"https://serviciosazure.abdigital.com:7443/servicesignalbazqa/api"
  //  host:"https://wsprodazure.abdigital.com:7443/servicesignalprd/api"
  }
};
