import { Component, OnInit, ChangeDetectorRef , ViewChild } from '@angular/core';
import { JsBase } from '../../core/base/JsBase';
import {MatDialog} from '@angular/material/dialog';
import * as signalR from "@microsoft/signalr";
import { DetalleLogin, Token } from 'src/app/core/interfaces/JsInterfaces';
import { ApiService } from 'src/app/service/api.service';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import {RemesasComponent} from 'src/app/modules/inicio/remesas/remesas.component';
import { ModalConfirmacionDialog } from 'src/app/shared/modal/modal-confirmacion/modal-confirmacion.dialog';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
 
})
//me voy toy con sueño :(
export class HeaderComponent extends JsBase implements OnInit {
  detalleLogin:DetalleLogin={} as DetalleLogin;
  connection:signalR.HubConnection;
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date =undefined;
  title = 'angular-idle-timeout';
  nombreUsuario:string;
  token: Token={} as Token;
  messages: signalR.Subject<string> = new signalR.Subject();
  constructor(private route: ActivatedRoute,private router: Router,private cdref: ChangeDetectorRef,private idle: Idle, private keepalive: Keepalive,public dialog: MatDialog,private api:ApiService) { 
    super(dialog);
      
    this.route.queryParams.subscribe(params => {
      this.token = params['request'];    
  });
    
    idle.setIdle(900);
  
    idle.setTimeout(15);
   
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => { 
      this.idleState = 'Ya no está inactivo.'
    
      this.reset();
    });
    
    idle.onTimeout.subscribe(() => {
      this.idleState = 'Se Acabo el Tiempo!';
      this.timedOut = true;
      localStorage.removeItem("token"); 
      localStorage.removeItem("token-expiracion");       
      sessionStorage.removeItem("UsuarioLogin"); 
      window!.top!.close;       
    });
    
    idle.onIdleStart.subscribe(() => {
        this.idleState = 'esta de vuelta!'   
    });
    
    idle.onTimeoutWarning.subscribe((countdown) => {
      this.idleState = 'You will time out in ' + countdown + ' seconds!' 
      
    });


    keepalive.interval(120);
    keepalive.onPing.subscribe(() => this.lastPing = new Date());
    this.reset();
  }

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;

  } 
  

  
  ngOnInit(): void {
     //
     this.inicio();
     this.start();
     ///
   

     ///
    this.api.getPaisesWU().subscribe((response) => {
      const cadena_corregida = JSON.stringify(
        response.detalle.Detalle
      ).replace(/\\\\/g, "\\"); 
      sessionStorage.setItem("PaisesWU",cadena_corregida)
      });


   // const uri='https://prodazure.abdigital.com:8443/WebLoginPrd';
   const uri='https://desarrolloazure.abdigital.com:8443/WebLoginQa/Menu';  
  if(sessionStorage.getItem("UsuarioLogin")!=null ){
    this.detalleLogin = <DetalleLogin>JSON.parse(<string>sessionStorage.getItem("UsuarioLogin"));
    if(this.detalleLogin.detalle?.estado.nombreRol!="Cajero"){
      this.onModalAdvertencia("Usuario no Autorizado");
      window.location.href =uri;
      return ;
    }      
  }  
  
    if(sessionStorage.getItem("UsuarioLogin")==null ){
      const token=JSON.stringify(this.token)
      if(token==null){     
        window.location.href =uri;
        return;
      }

   

     this.api.postPostLogin(this.token).subscribe(async(res)=>{   
       if(res==null){       
        window.location.href =uri;
        return;
      }       
      if (res.detalle.estado.nombreRol!="Cajero"){      
       
         window.location.href =uri;
         return ;
      }
      this.nombreUsuario= res.detalle.estado?.nombreUsuario;
      sessionStorage.setItem("UsuarioLogin", JSON.stringify(res));    
     // this.detalleLogin.detalle.estado.nombreUsuario=res.detalle.estado.nombreUsuario;
      })
    }
//this.detalleLogin = <DetalleLogin>JSON.parse(<string>sessionStorage.getItem("UsuarioLogin"));
//this.nombreUsuario= this.detalleLogin.detalle.estado?.nombreUsuario;    
 

   } 

   inicio(){    
    this.connection = new signalR.HubConnectionBuilder()   
    .withUrl("https://wsprodazure.abdigital.com:7443/servicesignalprd/hubcnx?apikey=27705C82-FCD3-43FC-AAB0-53E03569A342",{
    //.withUrl("https://serviciosazure.abdigital.com:7443/servicesignalbazqa/hubcnx?apikey=7B11D7F3-A747-4EBD-AC93-CC352E15HYTT9", {
      skipNegotiation: true,
      transport: signalR.HttpTransportType.WebSockets
    })
    .withAutomaticReconnect([0, 3000, 5000, 10000, 15000, 30000])
    .configureLogging(signalR.LogLevel.Information)
    .build(); 

  this.connection.start().catch((err) => console.error(err.toString()));

      this.connection.on("notify", (data: any) => {
        this.messages.next(data);
      });

}




  ngAfterViewInit() {
    this.connection = new signalR.HubConnectionBuilder() 
     .withUrl("https://wsprodazure.abdigital.com:7443/servicesignalprd/hubcnx?apikey=27705C82-FCD3-43FC-AAB0-53E03569A342")
   // .withUrl("https://serviciosazure.abdigital.com:7443/servicesignalbazqa/hubcnx?apikey=7B11D7F3-A747-4EBD-AC93-CC352E15HYTT9")
    .withAutomaticReconnect([0, 3000, 5000, 10000, 15000, 30000])
    .configureLogging(signalR.LogLevel.Information)
    .build();
    this.detalleLogin = <DetalleLogin>JSON.parse(<string>sessionStorage.getItem("UsuarioLogin"));
    this.nombreUsuario= this.detalleLogin.detalle?.estado.nombreUsuario//=null?this.detalleLogin.detalle.estado.nombreUsuario:this.detalleLogin.detalle.estado.nombreUsuario;
    this.cdref.detectChanges();
  }
 

  cerrarsession(){
    this.detalleLogin = <DetalleLogin>JSON.parse(<string>sessionStorage.getItem("UsuarioLogin"));
    this.onModalConfirmacion("Desea Cerrar Sessión");      
  }


async start(){ 
  const ReqSesion = 
  {    
  UserLogin: this.detalleLogin.detalle.estado.usuarioLogin,
  CodeSatelite:this.detalleLogin.detalle.estado.codigoSatelite ,                   
   SessionId:this.detalleLogin.detalle.estado.sessionId
  }
     try {
 await this.connection.start();
 const sessionid=this.detalleLogin.detalle.estado.sessionId
 console.log("SignalR Connected.");
 await this.connection.invoke("AddToGroup",sessionid);
 await this.connection.on("revokesession", (sessionRequest) => {
   console.log(">>>>>>>>>>>"+sessionRequest);
 localStorage.removeItem("token"); 
 localStorage.removeItem("token-expiracion"); 
 sessionStorage.removeItem("UsuarioLogin");
  this.api.postLogoutSatelite(ReqSesion).subscribe(async (res) => {  
});
window!.top!.close();
 });
 } catch (err) {
 setTimeout(this.start, 5000);
 }     
 }

 

 onModalConfirmacion(contenido: string) {
  const dialogRef = this.dialog.open(ModalConfirmacionDialog, {
    width: '400px',
    disableClose: true,
    data: {
      titulo: "Confirmación", 
      contenido: contenido,
      btnCancelar: {visible: true , texto: "Cancelar"},
      btnAceptar: {visible: true , texto: "Continuar"}
    }
  });
   
  dialogRef.afterClosed().subscribe(result => {
    if(result){
      ///
      const sessionId = 
      {    
      UserLogin: this.detalleLogin.detalle?.estado.usuarioLogin,
      CodeSatelite:this.detalleLogin.detalle?.estado.codigoSatelite ,                   
       SessionId:this.detalleLogin.detalle?.estado.sessionId
      }
      localStorage.removeItem("token"); 
      localStorage.removeItem("token-expiracion");  
      sessionStorage.removeItem("UsuarioLogin");   
     this.api.postLogoutSatelite(sessionId).subscribe(async (res) => {
      });  
      window!.top!.close(); 
      //
    }
  });
  }


  

}
