import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { Router, ActivatedRoute, ParamMap, Params} from '@angular/router';

import { JsBase } from '../../../core/base/JsBase';
import {MatDialog} from '@angular/material/dialog';
import { DetalleLogin, PaisesWU, Token } from 'src/app/core/interfaces/JsInterfaces';
import { ApiService } from 'src/app/service/api.service';
import * as signalR from "@microsoft/signalr";

@Component({
  selector: 'app-remesas',
  templateUrl: './remesas.component.html',
  styleUrls: ['./remesas.component.sass']
})
export class RemesasComponent extends JsBase implements OnInit {
     token: Token={} as Token;
  rutaActiva: any;
  lstCambioMoneda:Array<PaisesWU>=[];
  detalleLogin:DetalleLogin={}as DetalleLogin;
  connection:signalR.HubConnection;
  
  constructor(private api: ApiService,private router: Router, private route: ActivatedRoute, public dialog: MatDialog) {
    super(dialog);

   
    this.route.queryParams.subscribe(params => {
      this.token = params['request'];    
  });
   
   }






  ngOnInit(): void {

  localStorage.removeItem("ClienteBeanPagoTranslado");
  localStorage.removeItem("MTCNLista");
  localStorage.removeItem("MTCNListaWU");
  localStorage.removeItem("remesadatos");
  localStorage.removeItem("confirmaremesa");
  localStorage.removeItem("ClienteBeanEnvioTranslado");
  localStorage.removeItem("DetalleWu");
  localStorage.removeItem("estado");
  localStorage.removeItem("dinero");
  localStorage.removeItem("añoemi");
  localStorage.removeItem("añoact");
  localStorage.removeItem("año");
  localStorage.removeItem("ListaMoneda");
  localStorage.removeItem("ListaRemesaEnvio");

  this.detalleLogin = <DetalleLogin>JSON.parse(<string>sessionStorage.getItem("UsuarioLogin"));
  this.connection = new signalR.HubConnectionBuilder()   
 // .withUrl("https://wsprodazure.abdigital.com:7443/servicesignalprd/hubcnx?apikey=27705C82-FCD3-43FC-AAB0-53E03569A342")
  .withUrl("https://serviciosazure.abdigital.com:7443/servicesignalbazqa/hubcnx?apikey=7B11D7F3-A747-4EBD-AC93-CC352E15HYTT9")
  .withAutomaticReconnect([0, 3000, 5000, 10000, 15000, 30000])
  .configureLogging(signalR.LogLevel.Information)
  .build();

  }

 

  validarLogin(){

    const token=JSON.stringify(this.token)
    this.api.postPostLogin(this.token).subscribe((res)=>{   
      if (res.detalle.estado.nombreRol!="Cajero"){      
         this.onModalAdvertencia("Usuario no Autorizado");
         return false;
      }if(res==null){
        this.onModalAdvertencia("Usuario no iniciado")
        this.router.navigate(['https://desarrolloazure.abdigital.com:8443/WebLoginQa/Menu']);
      }

      return this.onModalInformacion("Bienvenido");
    
  })



  }

  onEnviarTransferencia(){
    this.router.navigate(['/envio-translado-busqueda']);
  }

  onPagarTransferencia(){
    this.router.navigate(['/pago-translado-busqueda']);
  }



  
}
